from PIL import Image
import json

# coordinates for dialog: z = 0.6 d = 0.7
# coordinates for center: z=0.35, d=0.299
# coordinates for right: z=0.7, d=0.299
# coordinates for left: z=0.05, d=0.299
# resize_width / resize_height for person - 0.3, 0.7
# main_resize - final image size; (x, y)


def photo_summary(path_to_main_photo, path_to_join_photo, z, d,
                  resize_width=0.3, resize_height=0.7, main_resize=(960, 537), save=False):
    main_img = Image.open(path_to_main_photo, mode='r')
    join_img = Image.open(path_to_join_photo)
    (first_width, first_height) = main_img.size
    second_width = int(first_width*resize_width)
    second_height = int(first_height*resize_height)
    pixdata = join_img.load()
    width, height = join_img.size
    join_img.save(path_to_join_photo, "PNG")
    mask = join_img.split()[3]
    for y in range(height):
        for x in range(width):
            if pixdata[x, y] == (255, 255, 255, 255):
                pixdata[x, y] = (255, 255, 255, 0)
    main_img.paste(join_img, (int(first_width*z), int(first_height*d)), mask=mask)
    if save is False:
        main_img.save('SilencedHouse\output.png')
    else:
        resized_img = main_img.resize(main_resize)
        resized_img.save('SilencedHouse\output.png')


if __name__ == '__main__':
    photo_summary(path_to_main_photo='SilencedHouse\First.png', path_to_join_photo='SilencedHouse\GraceS.png', z=0.05, d=0.299)
    #photo_summary(path_to_main_photo='SilencedHouse\Forest.png', path_to_join_photo='SilencedHouse\Grace.png',
          #        z=0.6, d=0.7, Dialog=True)
  #  photo_summary(path_to_main_photo='SilencedHouse\Forest2.png', path_to_join_photo='SilencedHouse\LloydS.png', z=0.7, d=0.299)